#include "textanalyzer.h"
#include <QFile>
#include <QTextStream>


QStringList TextAnalyzer::get_words(QString& file) {

    QStringList result;
    QFile qfile(file);
    if (!qfile.open(QIODevice::ReadOnly | QIODevice::Text))
        throw;

    QTextStream in(&qfile);
    in.setCodec("UTF-8");
    while (!in.atEnd()) {

        QString line = in.readLine();
        line = line.toLower();
        int i = 0;
        while (i < line.size()) {

            QString t = get_word(line, i);
            if (t.size() != 0) {

                result += t;

            }

        }

    }

    qfile.close();
    return result;

}


QString TextAnalyzer::get_word(QString& qs, int& i) {

    std::string s = qs.toUtf8().constData();
    QString result;
    while ((i < s.size()) && ((s[i] < 'a')|| (s[i] > 'z'))) {

        ++i;

    }

    while ((i < s.size()) && (s[i] >= 'a') && (s[i] <= 'z')) {

        result += s[i];
        ++i;
    }
    return result;

}


double TextAnalyzer::cos_similarity(QStringList& f, QStringList& r) {

    dict d;

    // fill for first text
    for (int i = 0; i < f.size(); ++i) {

        if ( d.find(f[i]) == d.end() ) {

            d[f[i]] = {1, 0};

        } else {

            d[f[i]].first += 1;

        }

    }

    // fill for second text
    for (int i = 0; i < r.size(); ++i) {

        if ( d.find(r[i]) == d.end() ) {

            d[r[i]] = {0, 1};

        } else {

            d[r[i]].second += 1;

        }


    }

    // count similarity
    double sim = 0;
    double l2_left = 0;
    double l2_right = 0;
    for (auto& x : d) {

        int left_usage = x.second.first;
        int right_usage = x.second.second;
        sim += left_usage * right_usage;
        l2_left += left_usage * left_usage;
        l2_right += right_usage * right_usage;

    }
    if ((l2_left != 0) && (l2_right != 0)) {

        return sim / sqrt(l2_left * l2_right);

    }
    else {

        return 0;

    }

}


double TextAnalyzer::count_similarity(QString& cur, QStringList& check) {

    double max_sim = 0;
    QStringList current_list;
    try {

        current_list = get_words(cur);

    }
    catch(...) {

        return 0;

    }

    for (int i = 0; i < check.size(); ++i) {

        QStringList t;
        try {

            t = get_words(check[i]);

        }
        catch(...) {

            // pass

        }

        double t_sim = cos_similarity(current_list, t);
        max_sim = t_sim > max_sim ? t_sim : max_sim;

    }
    return max_sim;

}
