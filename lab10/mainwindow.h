#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QStringListModel>
#include "unique_ptr.h"
#include "textanalyzer.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

private slots:

    void ChangeCurrentFileButtonPressed();

    void AddNewFileButtonPressed();
    void CheckCurrentFileButtonPressed();

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QStringList files_to_check;
    QString current_file;
    unique_ptr<QStringListModel> model_files_to_check;
    unique_ptr<QStringListModel> model_current_file;
    TextAnalyzer analyzer;

};
#endif // MAINWINDOW_H
