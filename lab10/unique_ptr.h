#ifndef UNIQUE_PTR_H
#define UNIQUE_PTR_H


template<class T>
class unique_ptr {
private:
    T* m_ptr;

public:
    unique_ptr(T* ptr = nullptr) : m_ptr(ptr) { }

    ~unique_ptr()
    {
        delete m_ptr;
    }
    unique_ptr(const unique_ptr& x) = delete;
    unique_ptr(unique_ptr&& x) : m_ptr(x.m_ptr) { x.m_ptr = nullptr; }
    unique_ptr& operator=(const unique_ptr& x) = delete;
    unique_ptr& operator=(unique_ptr&& x) {

        if (&x == this)
            return *this;


        delete m_ptr;

        m_ptr = x.m_ptr;
        x.m_ptr = nullptr;

        return *this;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
    bool isNull() const { return m_ptr == nullptr; }
    T* get_object() { return m_ptr; }

};
#endif // UNIQUE_PTR_H
