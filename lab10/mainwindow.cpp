#include "mainwindow.h"
#include "ui_mainwindow.h"


const int FontSize = 11;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QString("Проверка текста на плагиат"));

    connect(ui->pushButton, SIGNAL(clicked()),this, SLOT (AddNewFileButtonPressed()));
    connect(ui->pushButton_2, SIGNAL(clicked()),this, SLOT (ChangeCurrentFileButtonPressed()));
    connect(ui->pushButton_3, SIGNAL(clicked()),this, SLOT (CheckCurrentFileButtonPressed()));
    model_files_to_check = new QStringListModel(this);
    model_current_file = new QStringListModel(this);

    model_files_to_check->setStringList(files_to_check);
    model_current_file->setStringList(QStringList(current_file));

    ui->listView->setModel(model_files_to_check.get_object());
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listView_2->setModel(model_current_file.get_object());
    ui->listView_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->textBrowser_4->setFontPointSize(FontSize);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ChangeCurrentFileButtonPressed() {

    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setViewMode(QFileDialog::Detail);
    QStringList fileNames;
    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    if (fileNames.size() != 0) {

        current_file = fileNames[0];
        model_current_file->setStringList(QStringList(current_file));

    }

};

void MainWindow::AddNewFileButtonPressed() {

    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setViewMode(QFileDialog::Detail);
    QStringList fileNames;
    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    if (fileNames.size() != 0) {

        QString fileName = fileNames[0];
        bool find = false;
        for (int i = 0; (i < files_to_check.size()) && !find; ++i){

            find = files_to_check[i] == fileName ? true : find;

        }
        if (!find) {

            files_to_check << fileName;

        }
        model_files_to_check->setStringList(files_to_check);

    }

};

void MainWindow::CheckCurrentFileButtonPressed() {

    double result = 0;
    if (!(files_to_check.size() == 0 || current_file.size() == 0)) {

        result = analyzer.count_similarity(current_file, files_to_check);

    }
    ui->textBrowser_4->setText(QString::number(int(result * 100)) + QString(" %"));



}

