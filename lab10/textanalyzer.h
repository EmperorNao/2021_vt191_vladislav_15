#ifndef TEXTANALYZER_H
#define TEXTANALYZER_H

#include <QMainWindow>
#include <iostream>
#include <map>
typedef std::map<QString, std::pair<int, int>> dict;

class TextAnalyzer
{
private:
    QStringList get_words(QString& file);
    QString get_word(QString& s, int& i);
    double cos_similarity(QStringList& f, QStringList& r);

public:
    TextAnalyzer() {};
    double count_similarity(QString& cur, QStringList& check);
};

#endif // TEXTANALYZER_H
